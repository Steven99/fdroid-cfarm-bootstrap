execute playbook:

    ansible-galaxy install -f -r requirements.yml -p .galaxy
    ansible-playbook -i xxx.osuosl.org, provision.yml
