---
- hosts: all
  become_method: su

  tasks:
    - name: "apt: install debian packages for secure apt setup"
      apt:
        name: "{{item}}"
        state: latest
        install_recommends: no
        update_cache: yes
      with_items:
        - apt-transport-https
        - apt-transport-tor
        - debian-archive-keyring
        - gnupg
        - tor

    - name: "apt_repository: sgvtcaew4bxjd7ln.onion as first repo"
      apt_repository:
        repo: 'deb tor+http://sgvtcaew4bxjd7ln.onion/debian-security stretch/updates main'
        filename: 0.sgvtcaew4bxjd7ln.onion
        update_cache: no

    - name: "apt_repository: debian.osuosl.org stretch"
      apt_repository:
        repo: |
          deb https://debian.osuosl.org/debian/ stretch main
        update_cache: no

    - name: "apt_repository: debian.osuosl.org stretch-updates"
      apt_repository:
        repo: |
          deb https://debian.osuosl.org/debian/ stretch-updates main
        update_cache: no

    - name: "apt_repository: debian.osuosl.org stretch-backports"
      apt_repository:
        repo: |
          deb https://debian.osuosl.org/debian/ stretch-backports main
        update_cache: no

    - name: "apt_repository: deb.debian.org debian-security"
      apt_repository:
        repo: |
          deb https://deb.debian.org/debian-security/ stretch/updates main
        update_cache: no

    - name: "apt_repository: security.debian.org"
      apt_repository:
        repo: 'deb http://security.debian.org/debian-security stretch/updates main'
        update_cache: no

    - name: "copy: clear /etc/apt/sources.list"
      copy:
        content: ""
        dest: "/etc/apt/sources.list"

    - name: "copy: apt pinning rule for backports packages"
      copy:
        content: |
          Package: vagrant ruby-net-ssh
          Pin: release a=stretch-backports
          Pin-Priority: 500
        dest: /etc/apt/preferences.d/debian-stretch-backports.pref
      become: yes

    - name: "apt: dist-upgrade"
      apt:
        update_cache: yes
        upgrade: dist

    - name: "apt: install debian packages"
      apt:
        name: "{{item}}"
        state: latest
        autoclean: yes
        autoremove: yes
        install_recommends: no
      with_items:

        # essential utilities
        - bash-completion
        - elpa-markdown-mode
        - emacs-nox
        - emacs-goodies-el
        - git
        - htop
        - iotop
        - nethogs
        - lvm2
        - screen
        - unattended-upgrades
        - vim
        - yaml-mode

        # libvirt/KVM minimal
        - libvirt-clients
        - libvirt-daemon
        - libvirt-daemon-system
        - nfs-kernel-server
        - busybox
        - libguestfs-tools
        - bridge-utils
        - dnsmasq-base
        - ebtables
        - netcat-openbsd
        - qemu-kvm

        - vagrant
        - vagrant-mutate
        - vagrant-libvirt

        - python-libvirt
        - python-lxml

      become: yes

    - name: "file: losen permissions on /root for enabling vagrant nfs v4 support"
      file:
        path: /root
        mode: 0701

    - name: "file: create symbolic link to enable all locales"
      file:
        src: "/usr/share/i18n/SUPPORTED"
        dest: "/etc/locale.gen"
        state: link
        force: yes
    - name: "locale_gen: generate all locales"
      locale_gen: "name={{item}} state=present"
      with_lines:
        - "grep -Eo '^ *[^#][^ ]+' /etc/locale.gen"
      
    - name: "timezone: set system to Etc/UTC"
      timezone:
        name: Etc/UTC
      become: yes
    - name: 'lineinfile: set default system locale to en_US.UTF-8'
      lineinfile:
        dest: "/etc/default/locale"
        line: "LANG=en_US.UTF-8"
      become: yes

    - name: "include_role: ensure kvm nesting is enabled"
      include_role: name=ansible-debian-enable-kvm-nesting
      become: yes

    - lvol:
        vg: lvm
        lv: fdroidMirror
        size: 300G
    - filesystem:
        dev: /dev/lvm/fdroidMirror
        fstype: ext4
    - mount:
        path: /srv/mirror.f-droid.org
        src: /dev/lvm/fdroidMirror
        fstype: ext4
        state: mounted

    - lvol:
        vg: lvm
        lv: libvirtImagesPart
        size: 2T
    - filesystem:
        dev: /dev/lvm/libvirtImagesPart
        fstype: ext4
    - mount:
        path: /var/lib/libvirt/images
        src: /dev/lvm/libvirtImagesPart
        fstype: ext4
        state: mounted

    - name: "virt_pool: setup 'default' libvirt storage pool"
      virt_pool:
        name: default
        state: present
        xml: |
          <pool type="dir">
            <name>default</name>
            <source>
            </source>
            <target>
              <path>/var/lib/libvirt/images</path>
              <permissions>
                <mode>0711</mode>
                <owner>0</owner>
                <group>0</group>
              </permissions>
            </target>
          </pool>
    - name: "virt_pool: start 'default' libvirt storage pool"
      virt_pool:
        name: default
        state: active
    - name: "virt_pool: autostart 'default' libvirt storage pool"
      virt_pool:
        name: default
        autostart: yes
